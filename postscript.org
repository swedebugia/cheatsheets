#+AUTHOR:Joshua Branson
#+TITLE: Post Script
#+LATEX_HEADER: \usepackage{lmodern}
#+LATEX_HEADER: \usepackage[QX]{fontenc}
#+OPTIONS: H:10 toc:nil

* Post Script
Post script is a Turing complete programming language that is used to print documents or generate pdf documents.

ghostview is a way to view postscript files.
