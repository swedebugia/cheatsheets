#+AUTHOR:Joshua Branson
#+TITLE: Sway
#+LATEX_HEADER: \usepackage{lmodern}
#+LATEX_HEADER: \usepackage[QX]{fontenc}
#+OPTIONS: H:10 toc:nil

https://www.youtube.com/watch?v=j1I63wGcvU4

* Sway

may 5 sway and man sway are helpful.

After sway is running, sway [COMMAND] sends commands to sway.

man 7 xkeyboard-config   has lots of options that I can set for sway.  There's are the options that one sets with setxkbmap.

Model Apple laptop apple_laptop, Macbook/Macbook Pro

Options  ctrl:swapcaps


** man 5 sway

set variable value   cool for newer commands

clipboard content  store some content in the clipboard!

layout mode  sets the layout

layout auto   cycles between layouts

* commands I need to fix

M+s  to stack windows

M+w is tab mode


M+C+direction moves the window
