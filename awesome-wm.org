#+AUTHOR:Joshua Branson
#+TITLE: Awesome window Manager
#+LATEX_HEADER: \usepackage{lmodern}
#+LATEX_HEADER: \usepackage[QX]{fontenc}
#+OPTIONS: H:10 toc:nil

* Awesome Window Manager
** move clients to a different tag
client:move_to_tag
https://awesomewm.org/doc/api/classes/client.html#client:move_to_tag
** move client to screen
client:move_to_screen
https://awesomewm.org/doc/api/classes/client.html#client:move_to_screen
